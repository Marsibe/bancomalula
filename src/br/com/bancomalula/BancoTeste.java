package br.com.bancomalula;
/**
 * 
 * @author Marcos  Sim�es Bernardo
 *
 */
public class BancoTeste {

	public static void main(String[] args) {
		
		Conta.saldoDoBanco = 2_000_000.00;
		
		// Instancia uma nova conta
		Conta conta = new Conta(1, "Corrente", 2_750.00, "123", 
				new Cliente("Marcos", "26.687.612-2", "176.822.028-07", 
						"simoes.marcos@hotmail.com", "29041974", Sexo.MASCULINO));
		/*
		 * Exibe saldo da conta e do banco
		 */
		conta.exibeSaldo();
		conta.deposita(50);
		conta.exibeSaldo();
		System.out.println("Cofre: " + Conta.saldoDoBanco);
		conta.saca(100);
		conta.exibeSaldo();
		System.out.println("Cofre: " + Conta.saldoDoBanco);
		
		System.out.println();
		
		/*
		 *  Mostrar os dados da conta
		 */
		System.out.println("DADOS DA CONTA");
		System.out.println("N�       " + conta.getNumero());
		System.out.println("Tipo:    " + conta.getTipo());
		System.out.println("Senha:   " + conta.getSenha());
		System.out.println("Saldo:   " + conta.getSaldo());
		System.out.println("Titular: " + conta.getCliente().getNome());
		System.out.println("CPF:     " + conta.getCliente().getCpf());
		System.out.println("RG:      " + conta.getCliente().getRg());
		System.out.println("E-mail:  " + conta.getCliente().getEmail());
		System.out.println("Sexo:    " + conta.getCliente().getSexo().nome);
	}

}
