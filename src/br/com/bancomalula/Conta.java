package br.com.bancomalula;
/**
 * 
 * @author Marcos  Sim�es Bernardo
 *
 */
public class Conta {
	
	public static double saldoDoBanco;
	
	/*
	 * Atributos
	 */
	private int numero;
	private String tipo;
	private double saldo;
	private String senha;
	private Cliente cliente;
	
	/*
	 * Construtor
	 */
	public Conta(int numero, String tipo, double saldo, String senha, Cliente cliente) {
		this.numero = numero;
		this.tipo = tipo;
		this.saldo = saldo;
		this.senha = senha;
		this.cliente = cliente;
	}
	
	/*
	 * Getters e Setters
	 */
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	// M�todos
	public void exibeSaldo() {
		System.out.println(cliente.getNome() + " seu saldo � de R$ " + this.getSaldo());
	}
	
	public void saca(double valor) {
		this.saldo -= valor;
		Conta.saldoDoBanco -= valor;
	}
	
	public void deposita(double valor) {
		this.saldo += valor;
		Conta.saldoDoBanco += valor;
	}
	
	public void transferePara(Conta destino, double valor) {
		this.saca(valor);
		this.deposita(valor);
	}
}
